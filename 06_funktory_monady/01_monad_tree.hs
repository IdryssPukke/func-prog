import Control.Applicative

data Tree a = Empty | Leaf a | Node (Tree a) (Tree a)
	deriving (Show)

instance Functor Tree where
      fmap f Empty        = Empty
      fmap f (Leaf x)     = Leaf (f x)
      fmap f (Node l r) = Node (fmap f l) (fmap f r)

instance Applicative Tree where
   pure x = Leaf x
   (Leaf f) <*> (Leaf x) = Leaf (f x)
   (Leaf f) <*> (Node l r) = Node (fmap f l) (fmap f r)
   _ <*> _ = Empty



instance Monad Tree where
	return=Leaf
	t >>= f= mergeTrees(fmap f t)

mergeTrees:: Tree (Tree a) -> Tree a
mergeTrees Empty = Empty
mergeTrees (Leaf t) = t
mergeTrees (Node l r) = Node (mergeTrees l) (mergeTrees r)

myTree = Node 
			(Leaf 3)
			(Node
				(Node 
					(Leaf 1)
					(Node
						(Empty)
						(Node 
							(Empty)
						)
					)
				)
			)

