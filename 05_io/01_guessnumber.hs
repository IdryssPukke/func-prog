import System.Random

main = do
    num <- randomIO :: IO Int
    guessing (mod num 10) 3

guessing num n = do
  if (n<=0) then do putStrLn "Przegrales!"
  else do
          putStrLn "Wpisz swoja liczbe:"
          usr_answer <- getLine
          case compare (read usr_answer) num of
            LT -> do putStrLn "Za duza!"
                     guessing num (n-1)
            GT -> do putStrLn "za mala!"
                     guessing num (n-1)
            EQ -> putStrLn "Wygrales!"