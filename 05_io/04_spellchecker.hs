import System.IO
import Data.Char
import Data.List.Split
import Data.List

usun_znaki :: String -> String
usun_znaki xs = [x | x <- xs, notElem x ",.?!-:;\""]

main = do  
    contents <- readFile "/usr/share/dict/words"
    let linesOfFiles = lines $ map toLower contents

    contents_to_check <- readFile "text.txt"
    let word_to_check = words $ map toLower $ removePunctuation contents_to_check

    writeFile "nie_znaleziono.txt" $ concat $ intersperse "\n" $ filter (\el -> notElem el linesOfFiles) word_to_check