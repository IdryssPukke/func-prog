import System.IO
import Data.Char
 
main = do  
    contents <- readFile "text.txt"
    writeFile "text_upper.txt" $ map toUpper contents