﻿{-
Zdefiniuj funkcję supercyfra zgodnie z instrukcjami
Podpowiedzi:
* `div` i `mod` mogą się przydać
* podwójna rekurencja!  
-}
supercyfra :: Int -> Int
sumac :: Integer->Integer
sumac 0 = 0
sumac a = (a `mod` 10) + (sumac (a `div` 10))

superc :: Integer->Integer
superc a | a<0 = superc (abs a)
         | a<10 = a
         | otherwise = superc (sumac a)
