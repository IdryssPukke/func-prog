﻿{-
Napisz funkcję usunduplikaty, która usunie ze stringa powtarzające się znaki.
Podpowiedzi:
* string to lista
* przydatna może być funkcja usuwająca wszystkiego wystąpienia danego elementu z listy

PS zamiast 
    "usunduplikaty :: [Char] -> [Char]" 
mogłoby być 
    "usunduplikaty :: (Eq a) => [a] -> [a]"
Funkcja byłaby wtedy bardziej uniwersalna
-}
usunduplikaty :: [Char] -> [Char]
duplikaty :: [Char]->[Char]
duplikaty t = reverse(duplikat (reverse t))
duplikat :: [Char]->[Char]
duplikat [] = []
duplikat (h:t) | (elem h t) = duplikat t
		| otherwise = h : (duplikat t)
