﻿mojeLiczby = [f x | x <- lista, p x]
             where f = \a -> 2 * a         
                   lista = [1..10]          
                   p = \b -> b `mod` 2 == 0

mojeLiczby' = map f (filter p lista )
             where f = \a -> 2 * a
                   lista = [1..10]
                   p = \b -> b `mod` 2 == 0