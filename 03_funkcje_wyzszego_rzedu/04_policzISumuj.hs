﻿policzISumuj :: (Int -> Int) -> Int -> Int -> Int
policzISumuj f a b = foldl (+) 0 (map f [a..b])