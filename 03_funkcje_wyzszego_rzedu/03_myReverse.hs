﻿myReverse :: String -> String
myReverse "" = ""
myReverse s = l:myReverse e 
		where	l=last s
			e=init s